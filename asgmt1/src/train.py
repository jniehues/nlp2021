#!/usr/bin/python

import sys, argparse

from TextProcessor import TextProcessor;
from NGramLanguageModel import NGramLanguageModel
from Evaluator import Evaluator


def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--train", default="../data/ted/TED.en", type=str, help="text file containing the training data")
    parser.add_argument("--voc", default="../data/ted/TED.en.voc", type=str, help="text file containing the training data")
    parser.add_argument("--test_input", default="../data/ted/tst2015.input", type=str, help="text file containing the test data")
    parser.add_argument("--test_output", default="../data/ted/tst2015.en", type=str, help="text file containing the test data")

    args = parser.parse_args()

    """ Init components """
    """ Use your own langauge model and text processor"""
    processor = TextProcessor()
    lm = NGramLanguageModel(1)
    eval = Evaluator(lm,processor,args.voc)

    """ Load training data and train language model"""
    text = getText(args.train)

    """ Process text by the Text Processor"""
    prepro = processor.process(text)

    """ Process text by the Text Processor"""
    print ("Train language model ....")
    lm.train(prepro)
    print ("Language model trained")

    print ("Test data")
    test = getText(args.test_input)
    reference = getText(args.test_output)
    prepro_reference = processor.process(reference)
    lm.getPPL(prepro_reference)
    eval.eval(test,reference)


def getText(filename):
    f = open(filename,encoding="utf-8")

    text = [l.strip().split() for l in f.readlines()]
    text = [item for sublist in text for item in sublist]
    f.close()
    return text


if __name__ == "__main__":
   main()