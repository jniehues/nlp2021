#!/usr/bin/python

import argparse
import spacy
from spacy.tokens import Doc


class WhitespaceTokenizer:
    def __init__(self, vocab):
        self.vocab = vocab

    def __call__(self, text):
        words = text.split(" ")
        return Doc(self.vocab, words=words)

def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--input", default="../data/ted/tst2014.en", type=str, help="text file containing the training data")
    parser.add_argument("--output", default="../data/ted/tst2014.input", type=str, help="text file containing the training data")
    parser.add_argument("--voc", default="../data/ted/TED.en.voc", type=str, help="text file containing the valid data")

    args = parser.parse_args()

    """ Init components """
    """ Use your own langauge model and text processor"""

    voc = getVoc(args.voc)

    # The source pipeline with different components
    nlp = spacy.load("en_core_web_sm")
    nlp.tokenizer = WhitespaceTokenizer(nlp.vocab)

    f = open(args.input,encoding="utf-8")
    of = open(args.output,'w',encoding="utf-8")

    for l in f.readlines():
        doc = nlp(l.strip())
        out= []
        for token in doc:
            if(token.pos_ == "ADV" and token.text[-2:] == "ly" and token.text in voc and token.text[:-2] in voc and token.text != "only"):
                print("Change:",token.text," to ",token.text[:-2])
                out.append(token.text[:-2])

            else:
                out.append(token.text)
        of.write(" ".join(out)+"\n")

def getVoc(filename):
    f = open(filename,encoding="utf-8")

    return set([l.strip() for l in f.readlines()])


if __name__ == "__main__":
   main()