#!/usr/bin/python

import argparse
import nltk;
from FeatureCreator import FeatureCreator;
from DiffFeatureCreator import DiffFeatureCreator;
from BoWFeatureCreator import BoWFeatureCreator;
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics

def main():
    parser = argparse.ArgumentParser(description='Script to train question classifier')
    parser.add_argument("--train", default="../data/train.", type=str, help="prefix of training files")
    parser.add_argument("--test", default="../data/test.", type=str, help="prefix of test files")

    args = parser.parse_args()

    """ Init components """
    """ Create Feature set"""
    features = BoWFeatureCreator();

    """ Load training data and train language model"""
    train = getData(args.train)
    features.createFeatureSet(train)

    f,l = features.createFeatures(train)
    test = getData(args.test)
    testf,testl = features.createFeatures(test)

    # train the model on the features


    #predict the labels for the test data


    #output your results
    print(metrics.classification_report(testl, predicted));

def getData(filename):
    q1 = open(filename+"q1.txt",encoding="utf-8")
    q2 = open(filename+"q2.txt",encoding="utf-8")
    c = open(filename+"class.txt",encoding="utf-8")

    lq1 = q1.readline()
    lq2 = q2.readline()
    lc = c.readline()

    data = []

    while(lq1 and lq2 and lc):
        data.append((nltk.word_tokenize(lq1.strip()),nltk.word_tokenize(lq2.strip()),int(lc.strip())));
        lq1 = q1.readline()
        lq2 = q2.readline()
        lc = c.readline()
    return data


if __name__ == "__main__":
   main()