

awk -F '\t' '{if($6 == 1 || $6 == 0) print;}'  quora_duplicate_questions.tsv > quora_duplicate_questions.filtered.tsv
awk -F '\t' '{print $4}' quora_duplicate_questions.filtered.tsv > q1.txt
awk -F '\t' '{print $5}' quora_duplicate_questions.filtered.tsv > q2.txt
awk -F '\t' '{print $6}' quora_duplicate_questions.filtered.tsv > class.txt

head -n 40000 q1.txt > test.q1.txt
head -n 40000 q2.txt > test.q2.txt
head -n 40000 class.txt > test.class.txt

head -n 80000 q1.txt | tail -n 40000 > valid.q1.txt
head -n 80000 q2.txt | tail -n 40000 > valid.q2.txt
head -n 80000 class.txt | tail -n 40000 > valid.class.txt

tail -n +80001 q1.txt > train.q1.txt
tail -n +80001 q2.txt > train.q2.txt
tail -n +80001 class.txt > train.class.txt
