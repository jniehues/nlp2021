awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $2);b=0}}END{print("")}' eval-en.conllu > test.text
awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $4);b=0}}END{print("")}' eval-en.conllu > test.label

awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $2);b=0}}END{print("")}' test-en.conllu > valid.text
awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $4);b=0}}END{print("")}' test-en.conllu > valid.label

awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $2);b=0}}END{print("")}' train-en.conllu > train.text
awk -F '\t' 'BEGIN{b=1}{if($1 == 1 && b != 1) {print ("");}; if($0 !~ /^#/){printf("%s ", $4);b=0}}END{print("")}' train-en.conllu > train.label