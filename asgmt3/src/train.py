
import sys, argparse
import numpy as np
import keras
from keras.layers import Embedding
from keras.models import Sequential
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences


def main():
    parser = argparse.ArgumentParser(description='Script to train a language model')
    parser.add_argument("--data", default="../data/en/train", type=str, help="prefix for files containing training data")
    parser.add_argument("--valid", default="../data/en/valid", type=str, help="prefix for files containing valid data")
    parser.add_argument("--test", default="../data/en/test", type=str, help="prefix for files containing test data")


    args = parser.parse_args()


    """ Load data and split into training and testing"""
    """ List of dialogs, each dialog has many turns"""
    train = loadData(args.data)
    valid = loadData(args.valid)
    test = loadData(args.test)




    """ Preprocess the labels"""
    labels = {}
    name = {}
    name[0] = "PAD"
    c = 1
    for l in train[1]+valid[1]+test[1]:
        for w in l.split():
            if w not in labels:
                labels[w] = c
                name[c] = w
                c += 1

    print("Number of labels:",c);

    class_size = c

    train_labels = []
    for l in train[1]:
        train_labels.append([labels[w] for w in l.split()]);
    valid_labels = []
    for l in valid[1]:
        valid_labels.append([labels[w] for w in l.split()]);
    test_labels = []
    for l in test[1]:
        test_labels.append([labels[w] for w in l.split()]);


    """ Preprocessing of the text"""
    train_labels = pad_sequences([keras.utils.to_categorical(l, num_classes=class_size) for l in train_labels]);
    valid_labels = pad_sequences([keras.utils.to_categorical(l, num_classes=class_size) for l in valid_labels]);
    test_labels = pad_sequences(test_labels);


    num_words=20000
    tk = Tokenizer(num_words=num_words,filters='')
    tk.fit_on_texts(train[0])

    train_input = pad_sequences(tk.texts_to_sequences(train[0]))
    valid_input = pad_sequences(tk.texts_to_sequences(valid[0]))
    test_input = pad_sequences(tk.texts_to_sequences(test[0]))

    """ Create Model"""
    model = Sequential()


    embedding_layer = Embedding(num_words,
                                50, trainable=True,mask_zero=True)
    model.add(embedding_layer)


    model.add(keras.layers.TimeDistributed(keras.layers.Dense(class_size, activation='softmax')))

    model.summary();

    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    print('Training')
    model.fit(train_input, train_labels,
              batch_size=32,
              epochs=20,
              validation_data=(valid_input,valid_labels))

    print('Evaluation')
    predict_prob = model.predict(test_input,batch_size=32)
    predict_class = np.argmax(predict_prob, axis=2)


def loadData(prefix):


    input = loadFile(prefix+".text")
    label = loadFile(prefix+".label")
    return (input,label)

def loadFile(filename):

    f = open(filename)
    l = f.readlines()
    return [s.strip() for s in l]

if __name__ == "__main__":
   main()
